package by.gsu.igi.lectures.lecture02;

/**
 * Created by Evgeniy Myslovets.
 */
public class Dog {

    private String name;
    private int age;

    public Dog(String name) {
        this.name = name;
    }

    public void barking() {
        System.out.println("I'm barking");
    }

    public void sleeping() {
        int sleepingTime = 7;
        System.out.println("I'm sleeping for " + this.age + " years" +
                " every morning at " + sleepingTime + " a.m.");
    }

    public String getName() {
        return this.name;
    }

    public void setAge(int newAge) {
        this.age = newAge;
    }

    public int getAge() {
        return this.age;
    }
}
