package by.gsu.igi.students.JuliaPritychenko.lab2.part1;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        String nextString = scanner.next();
        System.out.println(nextString);
    }
}
