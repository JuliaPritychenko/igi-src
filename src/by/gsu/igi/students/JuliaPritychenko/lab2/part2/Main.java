package by.gsu.igi.students.JuliaPritychenko.lab2.part2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int temp;
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        System.out.print("Введите первое число ");
        int first = scanner.nextInt();
        System.out.print("Введите второе число ");
        int second= scanner.nextInt();
        temp = first + second;
        System.out.println(first + "+" + second + "=" + temp);
        temp = first - second;
        System.out.println(first + "-" + second + "=" + temp);
        temp = first * second;
        System.out.println(first + "*" + second + "=" + temp);
        temp = first / second;
        System.out.println(first + "/" + second + "=" + temp);
        temp = first % second;
        System.out.println(first + " mod " + second + "=" + temp);

    }
}
