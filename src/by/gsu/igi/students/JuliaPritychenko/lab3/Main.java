package by.gsu.igi.students.JuliaPritychenko.lab3;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        String speed = scanner.next();
        String kilometer = " kmh", meter = " ms", knot = " kn", mile = " mph";

        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(speed);
        if (matcher.find()) {
            String value = speed.substring(matcher.start(), matcher.end());
            Double result = Double.parseDouble(value);
            if (speed.contains(kilometer)) {
                result = result * 0.278;
                System.out.println(speed + "= " + result + meter);
            }
            if (speed.contains(meter)) {
                result = result * 1;
                System.out.println(speed + "= " + result + meter);
            }
            if (speed.contains(knot)) {
                result = result * 0.514;
                System.out.println(speed + "= " + result + meter);
            }
            if (speed.contains(mile)) {
                result = result * 0.447;
                System.out.println(speed + "= " + result + meter);
            }
        }
    }
}
