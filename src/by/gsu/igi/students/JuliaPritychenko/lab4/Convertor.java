package by.gsu.igi.students.JuliaPritychenko.lab4;

public class Convertor {
    public static double kilometer(double speed) {
        return speed *= 0.278;
    }

    public static double meter(double speed) {
        return speed *= 1;
    }

    public static double knot(double speed) {
        return speed *= 0.514;
    }

    public static double mile(double speed) {
        return speed *= 0.447;
    }
}
