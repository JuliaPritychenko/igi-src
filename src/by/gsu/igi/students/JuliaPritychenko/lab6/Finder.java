package by.gsu.igi.students.JuliaPritychenko.lab6;

public class Finder {
    public static int first(int times[]){
        int indexFirst=0;
        for (int i = 1; i < times.length; i++){
            if (times[i] < times[indexFirst]) {
                indexFirst = i;
            }
        }
        return indexFirst;
    }

    public static int second(int times[], int indexFirst){
        int indexSecond=0;
        for (int i = 1; i < times.length; i++) {
            if (i!=indexFirst && times[i] < times[indexSecond]) {
                indexSecond = i;
            }
        }
        return indexSecond;
    }
}
