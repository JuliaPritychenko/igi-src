package by.gsu.igi.students.JuliaPritychenko.lab6;

public class Marathon {
    public static void main(String[] arguments) {
        String[] names = {
                "Ленка", "Дима", "Сергей", "Андрэ",
                "Ольга", "Вика", "Юлек", "Эмма",
                "Леша", "Вера", "Анька", "Вадим",
                "Тимофей", "Иван", "Катя", "Павел"
        };

        int[] times = {
                341, 273, 278, 329, 445, 402, 388, 275,
                243, 334, 412, 393, 299, 343, 317, 265
        };

        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i] + ": " + times[i]);
        }
        int x = Finder.first(times);
        int y= Finder.second(times,x);
        System.out.println("Самый быстрый бегун - это " + names[x] + ": " + times[x]);
        System.out.println("На втором месте " + names[y] + ": " + times[y]);
    }
}
